// Copyright © 2017 Hardik Bagdi <hbagdi1@binghamton.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"bytes"
	"fmt"

	"github.com/google/go-github/github"
	"github.com/spf13/cobra"
)

var repo bool

// searchCmd represents the search command
var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Search github for repos,users",
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Println("search called")
		if len(args) == 0 {
			cmd.Usage()
			return nil
		}

		var buffer bytes.Buffer
		for i := 0; i < len(args); i++ {
			buffer.WriteString(args[i] + " ")
		}
		return searchRepos(buffer.String())
	},
}

var repoSearch = &cobra.Command{
	Use: "repos",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			cmd.Usage()
			return nil
		}

		var buffer bytes.Buffer
		for i := 0; i < len(args); i++ {
			buffer.WriteString(args[i] + " ")
		}
		return searchRepos(buffer.String())
	},
}

func searchRepos(query string) error {

	client := github.NewClient(nil)
	opt := &github.SearchOptions{
		ListOptions: github.ListOptions{PerPage: 100},
	}
	repoSearchResult, response, err := client.Search.Repositories(query, opt)
	if err != nil {
		return err
	}
	fmt.Println(response.NextPage)
	fmt.Println(*repoSearchResult.Total)
	fmt.Println(*repoSearchResult.IncompleteResults)
	return nil
}
func init() {
	searchCmd.AddCommand(repoSearch)
	RootCmd.AddCommand(searchCmd)
}
