// Copyright © 2017 Hardik Bagdi <hbagdi1@binghamton.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	"github.com/hardikbagdi/joe/util"
	"github.com/spf13/cobra"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "joe",
	Short: "Joe is pleasant.",
	Long:  `Joe is a CLI tool for interacting with Github.`,
	Run:   func(cmd *cobra.Command, args []string) {},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(chkConfig)
}

// chkConfig reads in config file if present.
func chkConfig() {
	err := util.LoadViperConfig()
	if err != nil {
		auth := util.NewAuth()
		err := setupAuth(auth)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		_, err := util.Token()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}

func setupAuth(auth *util.Auth) error {
	err := auth.DoAuth()
	if err != nil {
		return err
	}
	token := auth.Token()
	err = util.SetAPIToken(token)
	if err != nil {
		return err
	}
	err = util.SaveViperConfig()
	if err != nil {
		return err
	}
	return nil
}
