// Copyright © 2017 Hardik Bagdi <hbagdi1@binghamton.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package util

import (
	"fmt"

	"github.com/hardikbagdi/joe/github"
	"golang.org/x/crypto/ssh/terminal"
)

// Auth is used to get API token for basic authentication
type Auth struct {
	username, token string
}

// NewAuth return an empty Auth Struct
func NewAuth() *Auth {
	auth := new(Auth)
	return auth
}

// Token return token set for the auth.Username()
func (auth *Auth) Token() string {
	return auth.token
}

// Username return the username for whom the auth Struct
func (auth *Auth) Username() string {
	return auth.username
}

// DoAuth tries to authenticate the user
func (auth *Auth) DoAuth() error {
	fmt.Println("Seems like you've not setup Github with joe yet.")
	fmt.Println("Let's get started.")
	username, password := auth.getUserCredentials()
	token, err := github.BasicAuthAPIToken(username, password)
	if err != nil {
		return err
	}
	auth.token = token
	return nil
}

func (auth *Auth) getUserNameFromUser() string {
	username := ""
	for "" == username {
		fmt.Printf("Enter your Github username: ")
		fmt.Scanf("%s", &username)
	}
	return username
}

func (auth *Auth) getPasswordFromUser() string {
	password := ""
	for "" == password {
		fmt.Printf("Enter password: ")
		bytes, _ := terminal.ReadPassword(0)
		fmt.Println()
		password = string(bytes)
	}
	return password
}

func (auth *Auth) getUserCredentials() (string, string) {
	username := auth.getUserNameFromUser()
	password := auth.getPasswordFromUser()
	auth.username = username
	return username, password
}
